const apiDirection = 'https://rickandmortyapi.com/api/character';
const allCharactersUrl = `${apiDirection}/?page=`;
const characterByNameUrl = `${apiDirection}/?name=`;

export const getAllCharachters = async (page) => {
    let paginationUrl = allCharactersUrl + page;
    console.log(paginationUrl);
    try {
        const request = await fetch(paginationUrl, {
            method: 'GET',
        });
        const response = await request.json();
        if (!request.ok) {
            throw new Error(response.message);
        }
        return response;
    } catch (error) {
        console.log(error);
    }
};

export const getCharacterByGender = async (page, gender) =>
{

    let genderUrl = `${allCharactersUrl}${page}&gender=${gender}`;
    console.log("URL", genderUrl);
    try {
        const request = await fetch(genderUrl, {
            method: 'GET',
        });
        const response = await request.json();
        if (!request.ok) {
            throw new Error(response.message);
        }
        console.log("RESP",response);
        return response;
    } catch (error) {
        console.log(error);
    }
};

export const getCharacterByName = async (name) => {
    let nameUrl = characterByNameUrl + name;
    try {
        const request = await fetch(nameUrl, {
            method: 'GET',
        });
        const response = await request.json();
        if (!request.ok) {
            throw new Error(response.message);
        }
        return response;
    } catch (error) {
        console.log(error);
    }
};

export const getAllEpisodes = async (episodeUrl) => {
    try {
        const request = await fetch(episodeUrl, {
            method: 'GET',
        });
        const response = await request.json();
        if (!request.ok) {
            throw new Error(response.message);
        }
        return response;
    } catch (error) {
        console.log(error);
    }
};
