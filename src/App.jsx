import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { getAllCharachtersAsync } from './redux/slices/characters.slice';
import { getCharacterByGenderAsync } from './redux/slices/charGender.slice';
import { Home, MainPage } from './Pages';
import './App.scss';

function App()
{
    const dispatch = useDispatch();

        useEffect(() => {
        dispatch(getAllCharachtersAsync(1));
        dispatch(getCharacterByGenderAsync([1, 'male']));
        dispatch(getCharacterByGenderAsync([1, 'felame']));
        dispatch(getCharacterByGenderAsync([1, 'unknow']));
        dispatch(getCharacterByGenderAsync([1, 'genderless']));
        });

    return (
        <Router>
        <div className="App">
                <Routes>
                    <Route path="/" element={<Home />}></Route>
                    <Route
                        path="/main"
                        element={<MainPage />}
                    ></Route>
                </Routes>
            </div>
        </Router>
    );
}

export default App;
