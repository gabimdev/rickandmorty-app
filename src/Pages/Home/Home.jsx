import { useState } from 'react';
import { MainButton } from '../../Components';
import './Home.scss';
import logoRandM from '../../assets/Rick-And-Morty-Logo 1.png';
import logoSquad from '../../assets/LogoSquad.svg';


const Home = () =>
{

    const [btnInfo] = useState({
        text: "Continuar",
        link: "/main",
    })
    return (
        <>
            <div className="container">
                <div className="container__logo">
                    <img
                        className="container__logoSvg"
                        src={logoSquad}
                        alt="Logo Squadmakers"
                    />
                </div>
                <div className="container__display">
                    <img
                        className="container__img"
                        src={logoRandM}
                        alt="Logo Rick and Morty"
                    />
                </div>
                <div className="container__display-welcome">
                    <span className="container__welcome-text">
                        Bienvenido a Rick and Morty
                    </span>
                </div>

                <div className="container__display-text">
                    <span className="container__explain-text">
                        En esta prueba su capacidad ara construir la aplicación
                        mediante el análisis de códido y la reproducción del
                        seguiente diseño
                    </span>
                </div>
                <div className="container__button">
                    <MainButton data={btnInfo} />
                </div>
            </div>
            <div className="background"></div>
        </>
    );
};
export default Home;
