import { Header, CardDisplay } from '../../Components';
import './MainPage.scss';

const MainPage = () =>
{
    return (
        <>
            <div className="main">
                <Header />
                <CardDisplay />
                
            </div>
        </>
    );
};

export default MainPage;