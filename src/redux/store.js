import { configureStore } from '@reduxjs/toolkit';
import { loadState, saveState } from '../Utils/localSorage';

import { charachtersSlice } from './slices/characters.slice';
import { charGenderSlice } from './slices/charGender.slice';
import { pagination } from './slices/page.slice';


export const store = configureStore({
    reducer: {
        characters: charachtersSlice.reducer,
        charGender: charGenderSlice.reducer,
        pagination: pagination.reducer,
    },
    preloadedState: loadState(),
});

store.subscribe(() => saveState(store.getState()));