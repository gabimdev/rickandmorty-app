import { createSlice } from '@reduxjs/toolkit';


const INITIAL_STATE = {
    pageNumber: 1,
    filterName: "all",

};

export const pagination = createSlice({
    name: 'page',
    initialState: INITIAL_STATE,
    reducers: {
        increment: (state) =>
        {
            state.pageNumber += 1;
        },
        decrement: (state) =>
        {
            state.pageNumber -= 1;
        },
        reset: (state) =>
        {
            state.pageNumber = 1;
        },
        setFilterName: (state, action) =>
        {

            state.filterName = action.payload;
        },

    },
});

export const { increment, decrement, reset, setFilterName, setPageReady } = pagination.actions;

export default pagination.reducer

