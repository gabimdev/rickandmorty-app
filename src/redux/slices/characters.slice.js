import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { getAllCharachters } from '../../Api/rickandmortyapi';

const INITIAL_STATE = {
    all: [],
};


export const getAllCharachtersAsync = createAsyncThunk(
    'characters/getAllCharachters',
    async (p) => {
        return await getAllCharachters(p);
    }
);

export const charachtersSlice = createSlice({
    name: 'charachters',
    initialState: INITIAL_STATE,
    extraReducers: (builder) => {
        builder.addCase(getAllCharachtersAsync.fulfilled, (state, action) => {
            state.all = action.payload;
        });
    },
});

export const { addCharacters} = charachtersSlice.actions;

