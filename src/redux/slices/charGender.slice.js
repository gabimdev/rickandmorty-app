import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { getCharacterByGender } from '../../Api/rickandmortyapi';

const INITIAL_STATE = {
    charMale: [],
    charFemale: [],
    charUnknow: [],
    charGenderLess: [],
};


export const getCharacterByGenderAsync = createAsyncThunk(
    'characters/getCharacterByGender',
    async (data) =>
    {
        
        return await getCharacterByGender(data[0], data[1]);
    }
);

export const charGenderSlice = createSlice({
    name: 'charGender',
    initialState: INITIAL_STATE,
    extraReducers: (builder) =>
    {
        builder.addCase(getCharacterByGenderAsync.fulfilled, (state, action) =>
        {
            // eslint-disable-next-line default-case
            switch (action.meta.arg[1])
            {
                case 'male':
                    state.charMale = action.payload;
                    break;
                case 'female':
                    state.charFemale = action.payload;
                    break;
                case 'unknow':
                    state.charUnknow = action.payload;
                    break;
                case 'genderless':
                    state.charGenderLess = action.payload;
                    break;

            }
        });
    },
});

export const { addcharGender } = charGenderSlice.actions;