import { Pagination } from '../index'
import './Footer.scss';

const Footer = () =>
{

    return (
        <>
            <div className="footer">
                <Pagination/>
                <div className="footer__background"></div>
            </div>
        </>
    );
};

export default Footer;