import Filter from '../Filter/Filter';
import logoRandM from '../../assets/Rick-And-Morty-Logo 1.png';
import './Header.scss';

const Header = () => {
    return (
        <>
            <div className="header">
                <div className="header__container">
                    <div className="header__img">
                        <img
                            className="header__logo"
                            src={logoRandM}
                            alt="Logo Rick and Morty"
                        />
                    </div>
                    <nav className="header__nav">

                        <div className="header__filter">
                            <Filter/>
                        </div>
                    </nav>
                </div>
                <div className="header__background"></div>
            </div>
        </>
    );
};

export default Header;
