import './CharacterCard.scss';

const CharacterCard = (c) =>
{
    const char = c.char;



    return (
        <div className="card">
            <div className="card__item">
                <div className="photo">
                    <img src={char.image} alt={char.name} />
                </div>
                <div className="content">
                    <div className="content__head">
                        <div className="info">
                            <div className="info__status">
                                <span
                                    className="dot"
                                    style={{
                                        backgroundColor:
                                            char.status === 'Alive'
                                                ? 'green'
                                                : char.status === 'Dead'
                                                ? 'red'
                                                : '#9f9f9f',
                                    }}
                                ></span>
                                <span>{char.status}</span>
                                <span>-</span>
                                <span>{char.species}</span>
                            </div>
                            <div className="title">
                                <span>{char.name}</span>
                            </div>
                        </div>
                    </div>
                    <div className="content__body">
                        <div>Last know location:</div>
                        <span>{char.location.name}</span>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CharacterCard;
