import { useDispatch, useSelector } from 'react-redux';
import { decrement, increment, reset } from '../../redux/slices/page.slice';
import { getAllCharachtersAsync } from '../../redux/slices/characters.slice';
import { getCharacterByGenderAsync } from '../../redux/slices/charGender.slice';

import './Pagination.scss';



const Pagination = () => {
    const filterName = useSelector((state) => state.pagination.filterName);
    const pageNum = useSelector((state) => state.pagination.pageNumber);
   
    const dispatch = useDispatch();

    let maxPage;
    switch (filterName) {
        case 'male':
            maxPage = 31;
            break;
        case 'female':
            maxPage = 8;
            break;
        case 'unknow':
            maxPage = 3;
            break;
        case 'genderless':
            maxPage = 1;
            break;
        case 'all':
            maxPage = 42;
             break;
        default:
            break;
    }


    console.log(pageNum, filterName);
    const nextPage = async (page, filter) => {
        window.scrollTo(0, 0);
        page += 1;
        if (filter === 'all') {
            await dispatch(getAllCharachtersAsync(page));
            dispatch(increment());
        } else
        {
            console.log("FUNC", page,filter)
            await dispatch(getCharacterByGenderAsync([page, filter]));
            dispatch(increment());
        }
    };
    const previousPage = async (page, filter) => {
        window.scrollTo(0, 0);
        page -= 1;
        if (filter === 'all') {
            await dispatch(getAllCharachtersAsync(page));
            dispatch(decrement());
        } else {
            await dispatch(getCharacterByGenderAsync([page, filter]));
            dispatch(decrement());
        }
    };
    const resetPage = async (page, filter) => {
        window.scrollTo(0, 0);
        page = 1;
        if (filter === 'all') {
            await dispatch(getAllCharachtersAsync(page));
            dispatch(reset());
        } else {
            await dispatch(getCharacterByGenderAsync(page, filter));
            dispatch(reset());
        }
    };

    return (
        <>
            <div className="pagination">
                <div
                    class={
                        pageNum === 1
                            ? 'pagination__off'
                            : 'pagination__next'
                    }
                    onClick={() => previousPage(pageNum, filterName)}
                >
                    Anterior
                </div>
                <div
                    className="pagination__space"
                    onClick={() => resetPage(pageNum, filterName)}
                >
                    Reiniciar
                </div>
                <div
                    class={
                            pageNum === maxPage
                            ? 'pagination__off'
                            : 'pagination__previous'
                    }
                    onClick={() => nextPage(pageNum, filterName)}
                >
                    Siguiente
                </div>
            </div>
        </>
    );
};

export default Pagination;
