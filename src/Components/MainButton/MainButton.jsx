import { useNavigate } from 'react-router-dom';
import './MainButton.scss';

const MainButton = (props) => {
    const navigate = useNavigate();


    const handelOnClick = (direction) => {
        navigate(direction);
    };

    return (
        <>
            <button
                type="button"
                className="mainButton"
                onClick={() => handelOnClick(props.data.link)}
            >
                {props.data ? props.data.text : 'button'}
            </button>
        </>
    );
};

export default MainButton;
