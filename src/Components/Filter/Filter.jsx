import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { getAllCharachtersAsync } from '../../redux/slices/characters.slice';
import { getCharacterByGenderAsync } from '../../redux/slices/charGender.slice';
import { setFilterName, reset } from '../../redux/slices/page.slice';
import menu from '../../assets/menu.svg';
import './Filter.scss';
import React from 'react';

const Filter = () => {
    const [menuState, setMenuState] = useState(true);

    const handleMenu = () => {
        if (menuState) {
            setMenuState(false);
        } else {
            setMenuState(true);
        }
    };

    const dispatch = useDispatch();

    const getCharactersPagination = async (page, filter) => {
        await dispatch(getAllCharachtersAsync(page));
        window.scrollTo(0, 0);
        dispatch(setFilterName(filter));
        dispatch(reset());
        handleMenu();
    };

    const getCharacters = async (page, gender) => {
        dispatch(setFilterName(gender));
        window.scrollTo(0, 0);
        await dispatch(getCharacterByGenderAsync([page, gender]));
        dispatch(setFilterName(gender));
        dispatch(reset());
        handleMenu();
    };

    return (
        <>
            <div className={menuState ? 'filter' : 'filter__open'}>
                <div className={menuState ? 'filter__container' : 'open'}>
                    <span
                        className="filter__link active"
                        onClick={() => getCharactersPagination(1, 'all')}
                    >
                        All
                    </span>
                    <span
                        className="filter__link"
                        onClick={() => getCharacters(1, 'unknow')}
                    >
                        Unknow
                    </span>
                    <span
                        className="filter__link"
                        onClick={() => getCharacters(1, 'female')}
                    >
                        Female
                    </span>
                    <span
                        className="filter__link"
                        onClick={() => getCharacters(1, 'male')}
                    >
                        Male
                    </span>
                    <span
                        className="filter__link"
                        onClick={() => getCharacters(1, 'genderless')}
                    >
                        Genderless
                    </span>
                </div>
                <img
                    src={menu}
                    alt="menu filter"
                    className={
                        menuState ? 'filter__icon' : ' filter__icon close'
                    }
                    onClick={() => handleMenu()}
                />
            </div>
        </>
    );
};
export default Filter;
