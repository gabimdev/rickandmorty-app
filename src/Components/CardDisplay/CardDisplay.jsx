import { useSelector} from 'react-redux';
import { Footer, CharacterCard } from '../index';

import './CardDisplay.scss';


const CardDisplay = () => {
    const allCharacters = useSelector((state) => state.characters.all.results);

    const charactersMale = useSelector(
        (state) => state.charGender.charMale.results
    );
    const charactersFemale = useSelector(
        (state) => state.charGender.charFemale.results
    );
    const charactersUnknow = useSelector(
        (state) => state.charGender.charUnknow.results
    );
    const charactersGenderless = useSelector(
        (state) => state.charGender.charGenderLess.results
    );
    const filterName = useSelector((state) => state.pagination.filterName);
    let characterGender;
    switch (filterName) {
        case 'male':
            characterGender = charactersMale;
            break;
        case 'female':
            characterGender = charactersFemale;
            break;
        case 'unknow':
            characterGender = charactersUnknow;
            break;
        case 'genderless':
            characterGender = charactersGenderless;
            break;
        default:
            break;
    }

    return (
        <>
            {filterName !== 'all' ? (
                <div className="display">
                    {characterGender ? (
                        characterGender.map((character) => (
                            <CharacterCard
                                char={character}
                                key={character.id}
                            />
                        ))
                    ) : (
                        <div>error</div>
                    )}
                    <Footer />
                </div>
            ) : (
                <div className="display">
                    {allCharacters ? (
                        allCharacters.map((character) => (
                            <CharacterCard
                                char={character}
                                key={character.id}
                            />
                        ))
                    ) : (
                        <div>error</div>
                    )}
                    <Footer />
                </div>
            )}
        </>
    );
};

export default CardDisplay;
