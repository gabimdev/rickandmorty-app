# [Rick and Morty APP](https://rickandmorty-sm.netlify.app/) 

It is an APP made to consume the [rickandmorty api](https://rickandmortyapi.com/). The APP is deployed at [netlify](https://rickandmorty-sm.netlify.app/)  and was made with react and redux.


## APP folder structure

![folder structure](src/assets/folder_struct.png)


### Api

- rickandmortyapi - with functions to make requests to the api.


### Components

- CharacterCard - to render the characters received from the api.

- CardDisplay - to render the components CharacterCard.

- Header - header with navbar.

- Filter - the navbar with filters for the type of character.

- Footer - with the Pagination component.

- Pagination - component to change pages of characters.

- MainButton - a button component.


### Pages

- Welcome page - start page of the app.

- Main Page - the main page with all the app functions.

### redux

- slice - manage states of response form api and pagination.

### Styles

- with global css styles

### Utils

- localStorage - to save state to local storage.



## Installation

- First - clone the repo:

[https://gitlab.com/gabimdev/rickandmorty-app](https://gitlab.com/gabimdev/rickandmorty-app)

- Second - install dependencies:

```npm install```

- Third -run the server:

```npm start```


## Stack

- React

- Sass

- Redux


## License

[MIT](https://choosealicense.com/licenses/mit/)
